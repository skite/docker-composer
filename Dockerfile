FROM node:12 AS builder

WORKDIR front

COPY front .

RUN npm i && npm run ci-build-prod

FROM nginx:alpine

COPY nginx/nginx.conf /etc/nginx/nginx.conf
COPY --from=builder front/dist/Skite /var/www/html

CMD ["nginx", "-g", "daemon off;"]
